﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ValidadorImagen
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == MessageBox.Show("Esta seguro que desea salir","Advertencia",MessageBoxButtons.OKCancel))
            {
                this.Close();
            }
        }


        #region Metodos Basicos


        private DataTable ExecuteSP(SqlConnection conexion, string storeProcedure, Dictionary<string, object> parametro, bool isParametro = false)
        {

            SqlCommand comando = new SqlCommand(storeProcedure, conexion);
            foreach (KeyValuePair<string, object> aux in parametro)
                comando.Parameters.Add(new SqlParameter(aux.Key, aux.Value));
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandTimeout = 0;
            SqlDataReader objRead = comando.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(objRead);
            GC.Collect();
            return dt;
        }

        private DataTable ExecuteQuery(string query, Dictionary<string, object> parametro, bool isParametro = false)
        {
            SqlConnection conexion;

            //if (!isParametro)
            conexion = GetConexion();
            //else
            //    conexion = GetConexionParamerto();

            conexion.Open();

            SqlCommand comando = new SqlCommand(query, conexion);
            foreach (KeyValuePair<string, object> aux in parametro)
                comando.Parameters.Add(new SqlParameter(aux.Key, aux.Value));
            comando.CommandType = CommandType.Text;
            comando.CommandTimeout = 0;
            SqlDataReader objRead = comando.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(objRead);
            conexion.Close();
            conexion.Dispose();
            GC.Collect();
            return dt;
        }

        private SqlConnection GetConexion()
        {
            string Server = "209.208.26.40";
            string DatabaseName = "ecatchNEPS";
            string UserName = "sa";
            string Password = "E-capture";


            //string Server = "10.164.61.101";
            //string DatabaseName = "ecatchNEPS2";
            //string UserName = "applicationECapture";
            //string Password = "b7v4ERzFzUPCag6n";

            SqlConnection sConnection = new SqlConnection();
            sConnection.ConnectionString = @"Data Source=" + Server + ";Initial Catalog=" + DatabaseName + ";User ID=" + UserName + ";Password=" + Password + ";Connect Timeout=60000;MultipleActiveResultSets=True";
            GC.Collect();
            return sConnection;
        }




        #endregion

    }
}
